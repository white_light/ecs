#ifndef LIDHOLDER_H
#define LIDHOLDER_H

#include "DataBase/IDLocal.h"

class LIDHolder
{
public:
    virtual ~LIDHolder() = default;

    virtual void setId(const IDLocal &id) {
        this->id = id;
    }

    IDLocal getId() const {
        return id;
    }

private:
    IDLocal id;
};

#endif // LIDHOLDER_H
