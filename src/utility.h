#ifndef UTILITY_H
#define UTILITY_H

enum class COLOR { RED,
                   GREEN,
                   BLUE,
                   BLACK,
                   GRAY,
                   LIGHT_GRAY,
                   DARK_GREEN,
                   DARK_OLIVERGREEN,
                   OLIVEDRAB,
                   SEAGREEN,
                   YELLOW,
                   SADDLEBROWN };
void color_init(COLOR color);

struct Vect2 {

    Vect2()
        : x(0), y(0) {}
    Vect2(double x, double y)
        : x(x), y(y) {}

    Vect2 &operator +=(const Vect2 &v) {
        x += v.x;
        y += v.y;
        return *this;
    }

    Vect2 operator +(const Vect2 &v) {
        Vect2 result;
        result.x = x + v.x;
        result.y = y + v.y;
        return result;
    }

    Vect2 operator -(const Vect2 &v) {
        Vect2 result;
        result.x = x - v.x;
        result.y = y - v.y;
        return result;
    }

    Vect2 operator /(const double &s) {
        Vect2 result;
        result.x = x / s;
        result.y = y / s;
        return result;
    }

    Vect2 operator *(const double &s) {
        Vect2 result;
        result.x = x * s;
        result.y = y * s;
        return result;
    }

    double x, y;
};

namespace math {

Vect2 transform(const Vect2 &input, double angle, const Vect2 &moving);

}

#endif // UTILITY_H
