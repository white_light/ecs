#ifndef SYSTEMMANAGER_H
#define SYSTEMMANAGER_H

#include "Manager/Manager.h"
#include "Manager/ManagerStore/SystemManagerStore.h"

template<>
class Manager<syst::ISystem> : public ManagerStore<syst::ISystem>
{
public:
    void updateSystemsWith(const std::vector<syst::Role> &roles);
};

#endif // SYSTEMMANAGER_H
