#ifndef ENTITY_H
#define ENTITY_H

#include "Entity/EntityRoles.h"
#include "DataBase/LIDHolder.h"
#include "Component/Component.h"

#include <map>
#include <list>

namespace entity {

class IEntity : public LIDHolder
{
public:
    IEntity() = default;

    bool hasComponent(const comp::Role &role) const {
        return components.count(role);
    }

    bool hasComponents(const std::list<comp::Role> &roles) const {
        for(auto &role : roles) {
            if(!hasComponent(role))
                return false;
        }
        return true;
    }

    void addComponent(comp::IComponent *component) {
        components.insert(std::make_pair(component->getRole(), component));
    }

    template<comp::Role role>
    comp::Component<role> *getComponent() const {
        return dynamic_cast<comp::Component<role>*>(components.find(role)->second);
    }

    virtual Role getRole() const = 0;

protected:
    std::map<comp::Role, comp::IComponent*> components;
};

template<Role role>
class EntityBase : public IEntity
{
    using IEntity::IEntity;

    virtual Role getRole() const override {
        return role;
    }
};

template<Role role>
class Entity : public EntityBase<role>
{
    using EntityBase<role>::EntityBase;
};

}

#endif // ENTITY_H
