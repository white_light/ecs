#ifndef COMPONENTPHYSICSPROPERTYES_H
#define COMPONENTPHYSICSPROPERTYES_H


#include "Component/Component.h"
#include "utility.h"

namespace comp {

template<>
class Component<Role::PHYSICS_PROPERTYES> : public ComponentBase<Role::PHYSICS_PROPERTYES>
{
public:
    using ComponentBase<Role::PHYSICS_PROPERTYES>::ComponentBase;

//private:
    Vect2 velocity;
    Vect2 velocityMax;
    Vect2 acceleration;
    Vect2 accelerationMax;

    double angularVelocity = 0;
    double angularVelocityMax = 0;
    double angularAcceleration = 0;
    double angularAccelerationMax = 0;

    double mass = 0;
    double momentInertia = 0;
};

}

#endif // COMPONENTPHYSICSPROPERTYES_H
