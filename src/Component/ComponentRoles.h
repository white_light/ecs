#ifndef COMPONENTROLES_H
#define COMPONENTROLES_H

namespace comp {

enum class Role {
    POSITION,
    PHYSICS_PROPERTYES,
    RENDER,
    ENGIENS,
    ATTACHED_PHYSICS_VALUES,
    NONE
};

}

#endif // COMPONENTROLES_H
