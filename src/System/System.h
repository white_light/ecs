#ifndef SYSTEM_H
#define SYSTEM_H

#include "Manager/CommonManager.h"
#include "DataBase/LIDHolder.h"

namespace syst {

enum class Role {
    ENGINES_MAKE_STEP,
    RENDER,
    NONE
};

class ISystem : public LIDHolder
{
public:
    ISystem(CommonManagerECS *icommonManager)
        : commonManager(icommonManager)
    {}

    virtual void update() = 0;
    virtual Role getRole() const = 0;

protected:
    CommonManagerECS *commonManager;
};

template<Role role>
class SystemBase : public ISystem
{
public:
    using ISystem::ISystem;

    virtual Role getRole() const override {
        return role;
    }
};

template<Role role>
class System : public SystemBase<role>
{
public:
    using SystemBase<role>::SystemBase;
};

}

#endif // SYSTEM_H
