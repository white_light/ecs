#ifndef COMPONENT_H
#define COMPONENT_H

#include "Component/ComponentRoles.h"
#include "DataBase/LIDHolder.h"

namespace entity {
    class IEntity;
}

namespace comp {

class IComponent : public LIDHolder
{
public:
    IComponent(entity::IEntity *entity)
        : entity(entity)
    {}

    virtual Role getRole() const = 0;

protected:
    entity::IEntity *entity;
};

template<Role role>
class ComponentBase : public IComponent
{
public:
    using IComponent::IComponent;

    virtual Role getRole() const override {
        return role;
    }
};

template<Role role>
class Component : public ComponentBase<role>
{
public:
    using ComponentBase<role>::ComponentBase;
};

}

#endif // COMPONENT_H
