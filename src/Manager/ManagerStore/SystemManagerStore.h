#ifndef SYSTEMMANAGERSTORE_H
#define SYSTEMMANAGERSTORE_H

#include "Manager/ManagerStore/ManagerStore.h"
#include "System/System.h"

template<>
class ManagerStore<syst::ISystem> : public ManagerStoreBase<syst::ISystem, syst::Role>
{
public:
    template<syst::Role role>
    syst::System<role> *create(CommonManagerECS *commonManager) {
        auto s = new syst::System<role>(commonManager);
        add(s);
        return s;
    }
};

#endif // SYSTEMMANAGERSTORE_H
