#include "Manager/SystemManager.h"
#include "Entity/Entity.h"

void Manager<syst::ISystem>::updateSystemsWith(const std::vector<syst::Role> &roles) {
    for(auto s : activeSubjVect) {
        auto iter = std::find(roles.begin(), roles.end(), s->getRole());
        if(iter != roles.end()) {
            s->update();
        }
    }
}
