#include "Figure.h"

Figure::Figure(COLOR icolor,
               GLenum ilineType,
               float ilinesDrawingWidth) :
    colorCurrent(icolor),
    lineTypeCurrent(ilineType),
    linesDrawingWidth(ilinesDrawingWidth)
{

}
