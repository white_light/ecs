#include "Polygon2D.h"

Polygon2D::Polygon2D(std::vector<double> ivertices_coordinate,
                     COLOR icolor,
                     GLenum ilineType,
                     float ilinesDrawingWidth) :
    Figure(icolor, ilineType, ilinesDrawingWidth)
{
    for (size_t i = 0; i < ivertices_coordinate.size() - 1; i+=2)
    {
        Vect2 point{ivertices_coordinate[i], ivertices_coordinate[i+1]};
        pointsOriginal.push_back(point);
        points.push_back(point);
    }
}

void Polygon2D::draw()
{
    glEnable(GL_LINE_SMOOTH);
    color_init(colorCurrent);
    glBegin(lineTypeCurrent);
    for (auto iter = points.begin(); iter != points.end(); ++iter) {
        glVertex2f(iter->x, iter->y);
    }
    glEnd();

    if (linesDrawingWidth) {
        color_init(COLOR::BLACK);
        glLineWidth(linesDrawingWidth);
        glBegin(GL_LINE_LOOP);
        for (auto iter = points.begin(); iter != points.end(); ++iter) {
            glVertex2f(iter->x, iter->y);
        }
        glEnd();
    }

    glDisable(GL_LINE_SMOOTH);
}

void Polygon2D::recalculatePoints(const Vect2 &toPosition, const double &atAngle)
{
    for (size_t i = 0; i < pointsOriginal.size(); ++i) {
        points[i] = math::transform(pointsOriginal[i], atAngle, toPosition);
    }
}
