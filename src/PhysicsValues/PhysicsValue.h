#ifndef PHYSICSVALUE_H
#define PHYSICSVALUE_H

#include "PhysicsValues/Force.h"
#include "PhysicsValues/Torque.h"

#include <list>
#include <variant>

using PhysicsValue = std::variant<Force, Torque<double>, Torque<Force> >;
using PhysicsValuesList =  std::list<PhysicsValue>;

#endif // PHYSICSVALUE_H
