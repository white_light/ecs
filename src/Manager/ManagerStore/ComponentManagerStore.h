#ifndef COMPONENTMANAGERSTORE_H
#define COMPONENTMANAGERSTORE_H

#include "Manager/ManagerStore/ManagerStore.h"
#include "Component/Component.h"

template<>
class ManagerStore<comp::IComponent> : public ManagerStoreBase<comp::IComponent, comp::Role>
{
public:
    template<comp::Role role>
    comp::Component<role> *create(entity::IEntity *entity) {
        auto c = new comp::Component<role>(entity);
        add(c);
        return c;
    }
};

#endif // COMPONENTMANAGERSTORE_H
