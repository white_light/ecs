#ifndef MANAGERSTORE_H
#define MANAGERSTORE_H

#include "DataBase/IDLocal.h"

#include <vector>
#include <map>
#include <algorithm>

template<typename BaseType, typename RoleType>
class ManagerStoreBase {
public:
    ~ManagerStoreBase() {
        for(auto *subj : activeSubjVect) {
            delete subj;
        }

        for(auto *subj : notActiveSubjVect) {
            delete subj;
        }

        for(auto subj : activeSubjMap) {
            delete subj.second;
        }

        for(auto subj : notActiveSubjMap) {
            delete subj.second;
        }

        for(auto subj : activeSubjMultMap) {
            delete subj.second;
        }

        for(auto subj : notActiveSubjMultMap) {
            delete subj.second;
        }
    }

    void remove(const IDLocal &id) {
        deactive(id);
        auto pair = notActiveSubjMap.find(id);
        notActiveSubjMap.erase(id);
        notActiveSubjVect.erase(pair.second);
        notActiveSubjMultMap.erase(pair->second->getRole());
    }

    void active(const IDLocal &id) {
        if(notActiveSubjMap.count(id)) {
            auto pair = notActiveSubjMap.find(id);
            activeSubjMap.insert(*pair);
            activeSubjVect.push_back(pair->second);
            activeSubjMultMap.insert(std::make_pair(pair->second->getRole(), pair->second));

            notActiveSubjVect.erase(std::find(notActiveSubjVect.begin(), notActiveSubjVect.end(), pair->second));
            notActiveSubjMap.erase(id);
            notActiveSubjMultMap.erase(pair->second->getRole());
        } else {
            // TODO
        }
    }

    void deactive(const IDLocal &id) {
        if(activeSubjMap.count(id)) {
            auto pair = activeSubjMap.find(id);
            notActiveSubjMap.insert(*pair);
            notActiveSubjVect.push_back(pair.second);
            notActiveSubjMultMap.insert(std::make_pair(pair->second->getRole(), pair->second));

            activeSubjMap.erase(id);
            activeSubjVect.erase(pair.second);
            activeSubjMultMap.erase(pair->second->getRole());
        } else {
            // TODO
        }
    }

protected:
    void add(BaseType *subj) {
        subj->setId(IDLocal::generateIdLocal());
        notActiveSubjVect.push_back(subj);
        notActiveSubjMap.insert(std::make_pair(subj->getId(), subj));
        notActiveSubjMultMap.insert(std::make_pair(subj->getRole(), subj));
    }

    std::vector<BaseType*> activeSubjVect;
    std::vector<BaseType*> notActiveSubjVect;
    std::map<IDLocal, BaseType*> activeSubjMap;
    std::map<IDLocal, BaseType*> notActiveSubjMap;
    std::multimap<RoleType, BaseType*> activeSubjMultMap;
    std::multimap<RoleType, BaseType*> notActiveSubjMultMap;
};

template<typename BaseType>
class ManagerStore
{};

#endif // MANAGERSTORE_H
