#ifndef SYSTEMRENDER_H
#define SYSTEMRENDER_H

#include "System/System.h"

namespace syst {

template<>
class System<Role::RENDER> : public SystemBase<Role::RENDER>
{
public:
    using SystemBase<Role::RENDER>::SystemBase;

    virtual void update() override;
};

}

#endif // SYSTEMRENDER_H
