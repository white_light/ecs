#ifndef POLYGON2D_H
#define POLYGON2D_H

#include "Figure/Figure.h"

class Polygon2D : public Figure
{
public:
    Polygon2D(std::vector<double> ivertices_coordinate,
              COLOR icolor,
              GLenum ilineType,
              float ilinesDrawingWidth = 0);
    ~Polygon2D() override = default;

    void draw() override;
    void recalculatePoints(const Vect2 &toPosition, const double &atAngle) override;
};

#endif // POLYGON2D_H
