#include "GraphicEngine.h"

GraphicEngine::GraphicEngine(CommonManagerECS *commManager, QWidget *parent)
    : QGLWidget(parent),
      commManager(commManager)
{
    connect(&timerFrame, &QTimer::timeout, this, &GraphicEngine::drawFrame);
}

void GraphicEngine::start()
{
    show();
    timerFrame.start(10);
}

void GraphicEngine::drawFrame()
{
    updateGL();
}

void GraphicEngine::initializeGL()
{
    qglClearColor(Qt::black);
    glEnable(GL_DEPTH_TEST);
}

void GraphicEngine::resizeGL(int w, int h)
{
    GLfloat aspectRatio;
    if (h == 0)
        h = 1;
    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    aspectRatio = static_cast<GLfloat>(w) / static_cast<GLfloat>(h);
    if (w <= h) {
        windowWidth = 100;
        windowHeight = 100 / aspectRatio;
        glOrtho(-100.0, 100.0, static_cast<GLdouble>(-windowHeight), static_cast<GLdouble>(windowHeight), -4.0, 1.0);
    }
    else {
        windowWidth = 100 * aspectRatio;
        windowHeight = 100;
        glOrtho(static_cast<GLdouble>(-windowWidth), static_cast<GLdouble>(windowWidth), -100.0, 100.0, -4.0, 1.0);
    }
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    winWidth = w;
    winHeight = h;
}

void GraphicEngine::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

//    glBegin(GL_POLYGON);
//    glColor3ub(255, 0, 0);

//    glVertex3f(-10, -10, 1);
//    glVertex3f(10, -10, 1);
//    glVertex3f(10, 10, 1);
//    glVertex3f(-10, 10, 1);
//    glEnd();

//    glBegin(GL_POLYGON);
//    glColor3ub(0, 255, 0);

//    glVertex3f(0, 0, 2);
//    glVertex3f(20, 0, 2);
//    glVertex3f(20, -20, 2);
//    glVertex3f(0, -20, 2);
//    glEnd();

    commManager->updateSystemsWith({syst::Role::RENDER, syst::Role::ENGINES_MAKE_STEP});

//    commManager->applyToEntitiesWith({comp::Role::POSITION}, [](entity::IEntity *entity)
//    {
//        auto posComp = entity->getComponent<comp::Role::POSITION>();
//        posComp->position.x += 0.2;
//    });
}
