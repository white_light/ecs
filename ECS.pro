QT       += core gui network opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11 c++14 c++17

unix {
    LIBS += -lGL -lGLU -lglut
}

win32 {
    LIBS += -lopengl32 -lglu32 -lfreeglut
}

TARGET = ECS
TEMPLATE = app
INCLUDEPATH += src/

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        src/GraphicEngine.cpp \
        src/Component/ComponentPosition.cpp \
        src/DataBase/DataBase.cpp \
        main.cpp \
        src/DataBase/IDLocal.cpp \
        src/Engines/Engine.cpp \
        src/Figure/Figure.cpp \
        src/Figure/Polygon2D.cpp \
        src/Manager/SystemManager.cpp \
        src/PhysicsValues/Force.cpp \
        src/PhysicsValues/Torque.cpp \
        src/System/SystemEnginesMakeStep.cpp \
        src/System/SystemRender.cpp \
        src/utility.cpp

HEADERS += \
    src/GraphicEngine.h \
    src/Component/ComponentAttachedPhysValues.h \
    src/Component/ComponentEngines.h \
    src/Component/ComponentPhysicsPropertyes.h \
    src/Component/ComponentPosition.h \
    src/Component/ComponentRender.h \
    src/Engines/Engine.h \
    src/Figure/Figure.h \
    src/Figure/Polygon2D.h \
    src/Manager/CommonManager.h \
    src/Component/Component.h \
    src/Component/ComponentRoles.h \
    src/DataBase/DataBase.h \
    src/DataBase/ID.h \
    src/DataBase/IDLocal.h \
    src/DataBase/LIDHolder.h \
    src/Entity/Entity.h \
    src/Entity/EntityRoles.h \
    src/Manager/Manager.h \
    src/Manager/ManagerStore/ManagerStore.h \
    src/Manager/EntityManager.h \
    src/Manager/ComponentManager.h \
    src/Manager/ManagerStore/EntityManagerStore.h \
    src/Manager/ManagerStore/ComponentManagerStore.h \
    src/Manager/ManagerStore/SystemManagerStore.h \
    src/Manager/SystemManager.h \
    src/PhysicsValues/Force.h \
    src/PhysicsValues/PhysicsValue.h \
    src/PhysicsValues/Torque.h \
    src/System/System.h \
    src/System/SystemEnginesMakeStep.h \
    src/System/SystemRender.h \
    src/utility.h

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
