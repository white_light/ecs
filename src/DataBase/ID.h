#ifndef ID_H
#define ID_H

#include <QDataStream>

class ID {
public:
    ID(const bool valid = false)
        : valid(valid)
    {
        if(valid) {
            this->id = generateId();
        }
    }

    ID(const int &id)
        : id(id),
          valid(true)
    {
    }

    bool isValid() const {
        return valid;
    }

    int toInt() const {
        return id;
    }

    bool operator== (const ID &other) const {
        return this->id == other.id;
    }

    bool operator!= (const ID &other) const {
        return !(*this == other);
    }

    bool operator< (const ID &other) const {
        return this->id < other.id;
    }

    friend QDataStream &operator <<(QDataStream &stream, const ID &id) {
        stream << id.id
               << id.valid;
        return  stream;
    }

    friend QDataStream &operator >>(QDataStream &stream, ID &id) {
        stream >> id.id
               >> id.valid;
        return stream;
    }

private:
    static int generateId() {
        return ++idLast;
    }

    static int idLast;

    int id = 0;
    bool valid = false;
};

int ID::idLast = 0;

#endif // ID_H
