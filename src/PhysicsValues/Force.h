#ifndef FORCE_H
#define FORCE_H

#include "utility.h"

class Force
{
public:
    Force() = default;
    Force(double value, double angle, const Vect2 &attachmentPoint);

private:
    double value = 0;
    double angle = 0;
    Vect2 attachmentPoint;
};

#endif // FORCE_H
