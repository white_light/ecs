#ifndef COMPONENTENGINES_H
#define COMPONENTENGINES_H

#include "Component/Component.h"
#include "utility.h"

class IEngine;

#include <vector>

namespace comp {

template<>
class Component<Role::ENGIENS> : public ComponentBase<Role::ENGIENS>
{
public:
    using ComponentBase<Role::ENGIENS>::ComponentBase;


    std::vector<IEngine*> engines;
};

}

#endif // COMPONENTENGINES_H
