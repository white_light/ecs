#ifndef COMPONENTPOSITION_H
#define COMPONENTPOSITION_H

#include "Component/Component.h"
#include "utility.h"

namespace comp {

template<>
class Component<Role::POSITION> : public ComponentBase<Role::POSITION>
{
public:
    using ComponentBase<Role::POSITION>::ComponentBase;

//private:
    Vect2 position;
    double angle = 0;
    int level = -1;
};

}

#endif // COMPONENTPOSITION_H
