#ifndef COMPONENTRENDER_H
#define COMPONENTRENDER_H

#include "Component/Component.h"
#include "utility.h"
#include "Figure/Figure.h"

#include <vector>

namespace comp {

template<>
class Component<Role::RENDER> : public ComponentBase<Role::RENDER>
{
public:
    using ComponentBase<Role::RENDER>::ComponentBase;

//private:
    std::vector<Figure::SPtr> figures;
};

}

#endif // COMPONENTRENDER_H
