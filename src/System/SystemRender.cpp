#include "SystemRender.h"
#include "Manager/ComponentManager.h"
#include "Manager/EntityManager.h"
#include "Component/ComponentPosition.h"
#include "Component/ComponentRender.h"

#include <GL/freeglut.h>

namespace syst {

void syst::System<Role::RENDER>::update()
{
    commonManager->applyToEntitiesWith({comp::Role::RENDER}, [](entity::IEntity *entity)
    {
        auto renderComp = entity->getComponent<comp::Role::RENDER>();
        for(auto f : renderComp->figures) {
            f->draw();
        }
    });
}

}
