#ifndef SYSTEMENGINESMAKESTEP_H
#define SYSTEMENGINESMAKESTEP_H

#include "System/System.h"

namespace syst {

template<>
class System<Role::ENGINES_MAKE_STEP> : public SystemBase<Role::ENGINES_MAKE_STEP>
{
public:
    using SystemBase<Role::ENGINES_MAKE_STEP>::SystemBase;

    virtual void update() override;
};

}

#endif // SYSTEMENGINESMAKESTEP_H
