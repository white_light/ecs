#include "SystemEnginesMakeStep.h"
#include "Manager/EntityManager.h"
#include "Component/ComponentAttachedPhysValues.h"
#include "Component/ComponentEngines.h"
#include "Engines/Engine.h"

namespace syst {

void syst::System<Role::ENGINES_MAKE_STEP>::update()
{
    commonManager->applyToEntitiesWith({comp::Role::ENGIENS, comp::Role::ATTACHED_PHYSICS_VALUES}, [](entity::IEntity *entity)
    {
        auto enginesComp = entity->getComponent<comp::Role::ENGIENS>();
        auto attachedComp = entity->getComponent<comp::Role::ATTACHED_PHYSICS_VALUES>();

        for(auto e : enginesComp->engines) {
            e->makeStep(attachedComp);
        }
    });
}

}
