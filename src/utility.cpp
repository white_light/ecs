#include "utility.h"

#include <QGLWidget>
#include <QtMath>

void color_init(COLOR color)
{
    switch (color)
    {
    case COLOR::RED:
        glColor3ub(255, 0, 0);
        break;
    case COLOR::GREEN:
        glColor3ub(0, 255, 0);
        break;
    case COLOR::BLUE:
        glColor3ub(0, 0, 255);
        break;
    case COLOR::BLACK:
        glColor3ub(0, 0, 0);
        break;
    case COLOR::GRAY:
        glColor3ub(51, 51, 51);
        break;
    case COLOR::LIGHT_GRAY:
        glColor3ub(191, 191, 191);
        break;
    case COLOR::DARK_GREEN:
        glColor3ub(0, 100, 0);
        break;
    case COLOR::DARK_OLIVERGREEN:
        glColor3ub(85, 107, 47);
        break;
    case COLOR::OLIVEDRAB:
        glColor3ub(107, 142, 35);
        break;
    case COLOR::SEAGREEN:
        glColor3ub(46, 139, 87);
        break;
    case COLOR::YELLOW:
        glColor3ub(255, 255, 0);
        break;
    case COLOR::SADDLEBROWN:
        glColor3ub(139,69,19);
        break;

    default:
        break;
    }
}

namespace math {

Vect2 transform(const Vect2 &input, double angle, const Vect2 &moving)
{
    Vect2 result;
    result.x = input.x * cos(angle) - input.y * sin(angle) + moving.x;
    result.y = input.x * sin(angle) + input.y * cos(angle) + moving.y;
    return result;
}

}


