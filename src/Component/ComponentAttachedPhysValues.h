#ifndef COMPONENTATTACHEDPHYSVALUES_H
#define COMPONENTATTACHEDPHYSVALUES_H

#include "Component/Component.h"
#include "PhysicsValues/PhysicsValue.h"
#include "utility.h"

namespace comp {

template<>
class Component<Role::ATTACHED_PHYSICS_VALUES> : public ComponentBase<Role::ATTACHED_PHYSICS_VALUES>
{
public:
    using ComponentBase<Role::ATTACHED_PHYSICS_VALUES>::ComponentBase;

//private:
    PhysicsValuesList attachedValues;

    Force resultantForce;
    Torque<> resultantTorque;
};

}

#endif // COMPONENTATTACHEDPHYSVALUES_H
