#include "Torque.h"

Torque<double>::Torque(const double &value, const Vect2 &pointAbout)
    : value(value),
      pointAbout(pointAbout)
{
}

Torque<Force>::Torque(const Force &force, const Vect2 &pointAbout)
    : force(force),
      pointAbout(pointAbout)
{
}
