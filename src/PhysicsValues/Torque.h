#ifndef TORQUE_H
#define TORQUE_H

#include "PhysicsValues/Force.h"

template<typename ValueType = double>
class Torque {};

template<>
class Torque<double>
{
public:
    Torque() = default;
    Torque(const double &value, const Vect2 &pointAbout);

//    void setValue(const double &valueNew);
//    double getValue() const;
//    void setPointAbout(const Vect2 &point);

//    static double calcTorque(const Force &force, const Vect2 &pointAbout) {
//        auto forceProjection = force.inProjection();
//        auto shoulders = force.getAttachmentPoint() - pointAbout;
//        return math::vect2Multiply(shoulders, forceProjection);
//    }

private:
    double value;
    Vect2 pointAbout;
};

template<>
class Torque<Force>
{
public:
    Torque() = default;
    Torque(const Force &force, const Vect2 &pointAbout);

//    double getValue() const;
//    Force getForce() const;
//    void setForce(const Force &iforce);
//    void setPointAbout(const Vect2 &point);

private:
    Force force;
    Vect2 pointAbout;
};

#endif // TORQUE_H
