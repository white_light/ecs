#ifndef ENTITYMANAGERSTORE_H
#define ENTITYMANAGERSTORE_H

#include "Manager/ManagerStore/ManagerStore.h"
#include "Entity/Entity.h"

template<>
class ManagerStore<entity::IEntity> : public ManagerStoreBase<entity::IEntity, entity::Role>
{
public:
    template<entity::Role role>
    entity::Entity<role> *create() {
        auto e = new entity::Entity<role>();
        add(e);
        return e;
    }
};

#endif // ENTITYMANAGERSTORE_H
