#include "Force.h"

Force::Force(double value, double angle, const Vect2 &attachmentPoint)
    : value(value),
      angle(angle),
      attachmentPoint(attachmentPoint)
{
}
