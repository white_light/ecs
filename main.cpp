#include <QApplication>

#include "GraphicEngine.h"

#include <thread>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setAttribute(Qt::AA_UseDesktopOpenGL);

    CommonManager<entity::IEntity, comp::IComponent, syst::ISystem> commManager;
    auto graphic = new GraphicEngine(&commManager);

    //
    auto object1 = commManager.Manager<entity::IEntity>::create<entity::Role::OBJECT_TANK_BODY_SIMPLE>();
    auto compPos1 = commManager.Manager<comp::IComponent>::create<comp::Role::POSITION>(object1);
    auto compRend1 = commManager.Manager<comp::IComponent>::create<comp::Role::RENDER>(object1);
    auto polygon1 = new Polygon2D({-10,-10, 10,-10, 10,10, -10,10}, COLOR::GREEN, GL_POLYGON);
    compRend1->figures.push_back(Figure::SPtr(polygon1));
    compPos1->level = 1;

    object1->addComponent(compPos1);
    object1->addComponent(compRend1);
//
    auto object2 = commManager.Manager<entity::IEntity>::create<entity::Role::OBJECT_TANK_BODY_SIMPLE>();
    auto compPos2 = commManager.Manager<comp::IComponent>::create<comp::Role::POSITION>(object2);
    auto compRend2 = commManager.Manager<comp::IComponent>::create<comp::Role::RENDER>(object2);
    auto polygon2 = new Polygon2D({0,-50, 15,-35, 30,-50, 15,-65}, COLOR::RED, GL_POLYGON);
    compRend2->figures.push_back(Figure::SPtr(polygon2));
    compPos2->level = 1;
    auto compEngines2 = commManager.Manager<comp::IComponent>::create<comp::Role::ENGIENS>(object2);
    auto permEngine = new PermanentEngine;
    permEngine->force = Force(5, 90, {15, -50});
    compEngines2->engines.push_back(permEngine);
    auto compAttachedV2 = commManager.Manager<comp::IComponent>::create<comp::Role::ATTACHED_PHYSICS_VALUES>(object2);

    object2->addComponent(compPos2);
    object2->addComponent(compRend2);
    object2->addComponent(compEngines2);
    object2->addComponent(compAttachedV2);

    //
    auto systRend = commManager.Manager<syst::ISystem>::create<syst::Role::RENDER>(&commManager);
    auto systEnginesMakeStep = commManager.Manager<syst::ISystem>::create<syst::Role::ENGINES_MAKE_STEP>(&commManager);

    commManager.Manager<entity::IEntity>::active(object1->getId());
    commManager.Manager<comp::IComponent>::active(compPos1->getId());
    commManager.Manager<comp::IComponent>::active(compRend1->getId());

    commManager.Manager<entity::IEntity>::active(object2->getId());
    commManager.Manager<comp::IComponent>::active(compPos2->getId());
    commManager.Manager<comp::IComponent>::active(compRend2->getId());
    commManager.Manager<comp::IComponent>::active(compEngines2->getId());
    commManager.Manager<comp::IComponent>::active(compAttachedV2->getId());

    commManager.Manager<syst::ISystem>::active(systRend->getId());
    commManager.Manager<syst::ISystem>::active(systEnginesMakeStep->getId());

    graphic->start();

//    while (1) {

//        std::this_thread::sleep_for(std::chrono::milliseconds(100));
//    }

    return a.exec();
}
