#ifndef ENTITYMANAGER_H
#define ENTITYMANAGER_H

#include "Manager/Manager.h"
#include "Manager/ManagerStore/EntityManagerStore.h"

#include <functional>

template<>
class Manager<entity::IEntity> : public ManagerStore<entity::IEntity>
{
public:
    void applyToEntitiesWith(const std::list<comp::Role> &roles, const std::function<void(entity::IEntity *)> &func) {
        for(auto iter = activeSubjMap.begin(); iter != activeSubjMap.end(); ++iter) {
            if(iter->second->hasComponents(roles)) {
                func(iter->second);
            }
        }
    }
};

#endif // ENTITYMANAGER_H
