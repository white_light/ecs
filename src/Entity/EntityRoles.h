#ifndef ENTITYROLES_H
#define ENTITYROLES_H

namespace entity {

enum class Role {
    OBJECT_TANK,
    OBJECT_TANK_BODY_SIMPLE,
    OBJECT_TANK_TOWER_SIMPLE,
    NONE
};

}

#endif // ENTITYROLES_H
