#ifndef COMMONMANAGER_H
#define COMMONMANAGER_H

#include "Manager/Manager.h"

namespace entity {
    class IEntity;
}

namespace syst {
    class ISystem;
}

namespace comp {
    class IComponent;
}

template<typename ...ManagerTypes>
class CommonManager : public Manager<ManagerTypes>...
{

};

using CommonManagerECS = CommonManager<entity::IEntity, comp::IComponent, syst::ISystem>;

#endif // COMMONMANAGER_H
