#ifndef IDLOCAL_H
#define IDLOCAL_H

class IDLocal
{
public:
    IDLocal() = default;

    IDLocal(const int &idLocal)
        : idLocal(idLocal),
          valid(true)
    {}

    bool isValid() const {
        return valid;
    }

    bool operator< (const IDLocal &other) const {
        return this->idLocal < other.idLocal;
    }

    bool operator== (const IDLocal &other) const {
        return this->idLocal == other.idLocal;
    }

    static int generateIdLocal() {
        return ++idLast;
    }

protected:
    static int idLast;

    int idLocal = 0;
    bool valid = false;
};

#endif // IDLOCAL_H
