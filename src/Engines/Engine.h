#ifndef ENGINE_H
#define ENGINE_H

#include "PhysicsValues/Force.h"
#include "Component/Component.h"

class IEngine {
public:
    virtual void makeStep(comp::Component<comp::Role::ATTACHED_PHYSICS_VALUES> *attachedComp) = 0;
};

class PermanentEngine : public IEngine {
public:
    virtual void makeStep(comp::Component<comp::Role::ATTACHED_PHYSICS_VALUES> *attachedComp) override;

    Force force;
};

#endif // ENGINE_H
