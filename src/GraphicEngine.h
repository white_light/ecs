#ifndef GRAPHICENGINE_H
#define GRAPHICENGINE_H

#include "Manager/CommonManager.h"
#include "Manager/ComponentManager.h"
#include "Manager/EntityManager.h"
#include "Manager/SystemManager.h"

#include "Component/ComponentRender.h"
#include "Component/ComponentPosition.h"
#include "Component/ComponentPhysicsPropertyes.h"
#include "Component/ComponentAttachedPhysValues.h"
#include "Component/ComponentEngines.h"
#include "System/SystemRender.h"
#include "System/SystemEnginesMakeStep.h"
#include "Engines/Engine.h"

#include "Figure/Polygon2D.h"

#include <QGLWidget>
#include <QTimer>

class GraphicEngine : public QGLWidget
{
    Q_OBJECT
public:
    GraphicEngine(CommonManagerECS *commManager,
                  QWidget *parent = nullptr);
//    void setKeyController(const std::shared_ptr<KeyController> &keyController);

    void start();
//    Q_INVOKABLE void stop();

protected:
    void drawFrame();
//    Vect3 getCursorData() const;

    // QWidget interface
//    void mousePressEvent(QMouseEvent *event) override;
//    void wheelEvent(QWheelEvent *event) override;
//    void keyPressEvent(QKeyEvent *event) override;
//    void keyReleaseEvent(QKeyEvent *event) override;

    // QGLWidget interface
    void initializeGL() override;
    void resizeGL(int w, int h) override;
    void paintGL() override;

private:
    QTimer timerFrame;
    CommonManagerECS *commManager;
//    std::shared_ptr<KeyController> keyController;
//    DataBase *dataBase;
//    std::shared_ptr<Object> clientPlayer;

    GLfloat windowWidth = 100, windowHeight = 100; //для resize
    int winWidth = 100, winHeight = 100;
};
#endif // GRAPHICENGINE_H
