#ifndef FIGURE_H
#define FIGURE_H

#include "DataBase/LIDHolder.h"
#include "utility.h"

#include <vector>
#include <memory>
#include <GL/freeglut.h>

class Figure : public LIDHolder
{
public:
    using SPtr = std::shared_ptr<Figure>;

    Figure(COLOR icolor,
           GLenum ilineType,
           float ilinesDrawingWidth);
    virtual ~Figure() override = default;
    virtual void draw() = 0;
    virtual void recalculatePoints(const Vect2 &toPosition, const double &atAngle) = 0;

protected:
    std::vector<Vect2> pointsOriginal;
    std::vector<Vect2> points;

    COLOR colorCurrent;
    GLenum lineTypeCurrent;
    float linesDrawingWidth;
};

#endif // FIGURE_H
